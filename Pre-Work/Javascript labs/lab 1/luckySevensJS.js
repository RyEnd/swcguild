var startingBet = (document.getElementById("startingBet").value);
var highestHeld = startingBet;
var totalRolls = 0;
var rollsMaxMoney = 0;

function playGame() {
	function clearTable() {
		document.getElementById("startingBetResult").innerHTML = "";
		document.getElementById("totalRollsResult").innerHTML = "";	
		document.getElementById("highestHeldResult").innerHTML = "";
		document.getElementById("rollsHighestHeldResult").innerHTML = "";
	};
	var diceRolls = 0;
	var maxMoney = (document.getElementById("startingBet").value);
	var bet = (document.getElementById("startingBet").value);
	if (bet <= 0) {
		return alert("Please place your bet.");
	}
	if (isNaN(bet)) {
		return alert("Please bet a number.");
	}
	while (bet > 0) {
		var d1 = Math.floor(Math.random() * 6) + 1;
		var d2 = Math.floor(Math.random() * 6) + 1;
		var dTotal = d1 + d2;
		diceRolls++;
		if (dTotal == 7) {
			bet += 4;
		}
		else {
			bet -= 1;
		}
		if (bet >= maxMoney) {
			highestHeld = maxMoney = bet;
			rollsMaxMoney = diceRolls;
		}
		else if (bet <= 0) {
			totalRolls = diceRolls;
			results();
			playAgain();
		}
	}
}

function results() {
	document.getElementById("startingBetResult").innerHTML = (document.getElementById("startingBet").value);
	document.getElementById("totalRollsResult").innerHTML = totalRolls;
	document.getElementById("highestHeldResult").innerHTML = "$" + highestHeld;
	document.getElementById("rollsHighestHeldResult").innerHTML = rollsMaxMoney;
}

function playAgain() {
	document.getElementById("playButton").innerHTML = "Play Again?";
}

function unhide(id) {
	var item = document.getElementById(id);
	if (item.style.display = "none") {
		item.style.display = "block";
	}
	else {
		item.style.display = "block";
	}
}